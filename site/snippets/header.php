<!doctype html>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title><?= $site->title()->html() ?> | <?= $page->title()->html() ?></title>
  <meta name="description" content="<?= $site->description()->html() ?>">

  <?= css(c::get('siteurl').'/assets/css/main.css') ?>
  <?= css(c::get('siteurl').'/assets/css/footer.css') ?>
  <?= css(c::get('siteurl').'/assets/icomoon/style.css') ?>

  <?= css('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/reset.min.css') ?>
  <?= css('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/grid.min.css') ?>
  <?= css('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/button.min.css') ?>
  <?= css('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/container.min.css') ?>
  <?= css('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/menu.min.css') ?>
  <?= css('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/dropdown.min.css') ?>
  <?= js('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/dropdown.min.js') ?>
  <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
</head>
<body>

  <header class="header ui container" role="banner">

      <?php snippet('menu') ?>

  </header>
