  <footer class="ui container" role="contentinfo">

  <div>
    <div class="left">
      <div class="item" class="pb5" style="display:flex;align-items:center;">
        <img style="width:120px;height: auto;" src="/assets/images/logo-wide.png" />
      </div>
      <div class="ui list left">
        <div class="item">
          <i class="users icon"></i>
          <div class="content">
            Pithia Inc
          </div>
        </div>
        <div class="item pb10">
          <i class="marker icon"></i>
          <div class="content">
            Seattle, WA
          </div>
        </div>
        <div class="item">
          <div class="content">
            <i class="icon-envelop"></i>
            <a href="mailto:contact@pithia.com">contact@pithia.com</a>
          </div>
        </div>
        <div class="item">
          <div class="content">
            <i class="icon-envelop"></i>
            <a href="mailto:invest@pithia.com">invest@pithia.com</a>
          </div>
        </div>
      </div>
    </div>
  
    <div class="ui list right">
      <div class="item">
        <div class="content">
        <i class="icon-telegram"></i>
          <a href="#telegram">Telegram</a>
        </div>
      </div>
      <div class="item">
        <div class="content">
        <i class="icon-reddit"></i>
          <a href="#reddit">Reddit</a>
        </div>
      </div>
    </div>
  </div>
  <!--
      <p class="footer-copyright">
        // Parse Kirbytext to support dynamic year,
        // but remove all HTML like paragraph tags:
        echo html::decode($site->copyright()->kirbytext())
      </p>-->


    
    <!--
      <div class="logo-div">
        <img src="/assets/images/logo.png" />
      </div>-->
  </footer>

</body>
</html>