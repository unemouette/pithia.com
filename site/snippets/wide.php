<?php if($page): ?>
<div class="wide" style="background-image:url(<?= $page->content()->image() ?>)">
    <div class="ui container">
        <div>
            <?php if(strlen($page->text())): ?>
                <h1><?= $page->text() ?></h1>
            <?php endif ?>
            <?php if(strlen($page->intro()->html())): ?>
                <div class="centered text intro"><?= $page->intro()->kirbytext() ?></div>
            <?php endif ?>
            <?php if(strlen($page->content()->link()) > 0 && strlen($page->content()->linktext()) > 0): ?>
                <div class="pt10 pb10">
                    <a href="<?= $page->content()->link() ?>" class="ui button inverted"><?= $page->content()->linktext() ?></a>
                </div>
            <?php endif ?>
            <?php if($page->hasChildren()): ?>
                <div class="items">
                    <?php foreach($page->children() as $child): ?>
                        <?= snippet('wide-item', ['title' => $child->content()->title(), 'text' => $child->content()->text(), 'image' => $child->content()->image()]) ?>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<?php endif ?>