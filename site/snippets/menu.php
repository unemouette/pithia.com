<?php /*
<nav class="navigation column" role="navigation">
  <ul class="menu">
    <?php foreach($pages->visible() as $item): ?>
    <li class="menu-item<?= r($item->isOpen(), ' is-active') ?>">
      <a href="<?= $item->url() ?>"><?= $item->title()->html() ?></a>
    </li>
    <?php endforeach ?>
  </ul>
</nav>
*/ ?>

<div class="ui secondary menu">
  <div class="item" style="display:flex;align-items:center;">
    <img style="width:120px;height: auto;" src="/assets/images/logo-wide.png" />
  </div>
  <a class="item <?php echo($page->id() === 'home' ? 'active' : ''); ?>" href="/" >Home</a>
  <div class="ui simple dropdown item">
    Principles
    <i class="dropdown icon"></i>
    <div class="menu">
      <a class="item <?php echo($page->id() === 'what-we-do' ? 'active' : ''); ?>" href="/what-we-do">What we do</a>
      <a class="item <?php echo($page->id() === 'how-to-invest' ? 'active' : ''); ?>" href="/how-to-invest">How to invest</a>
      <a class="item <?php echo($page->id() === 'new-entrepreneurs' ? 'active' : ''); ?>" href="/new-entrepreneurs">For new entrepreneurs</a>
    </div>
  </div>

    <div class="ui simple dropdown item">
   About us
    <i class="dropdown icon"></i>
    <div class="menu">
      <a class="item <?php echo($page->id() === 'funds' ? 'active' : ''); ?>" href="/funds">Funds</a>
      <a class="item <?php echo($page->id() === 'working-groups' ? 'active' : ''); ?>" href="/working-groups">Working groups</a>
      <a class="item <?php echo($page->id() === 'partners' ? 'active' : ''); ?>" href="/partners">Partners</a>
    </div>
  </div>
  <a class="item <?php echo($page->id() === 'team' ? 'active' : ''); ?>" href="/team" >Team</a>
  <a class="item <?php echo($page->id() === 'careers' ? 'active' : ''); ?>" href="/careers" >Careers</a>
</div>