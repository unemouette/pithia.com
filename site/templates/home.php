<?php snippet('header') ?>

<?php snippet('wide', ['page' => page('home')->children()->findByURI('home-wide-01')]) ?>

<main class="ui container pt40 pb40" role="main">
    
  <h1 class="title"><?= $page->title1()->html() ?></h1>
  <div class="pt20 pb20">
    <?= $page->content()->text1()->kirbytext() ?>
  </div>

</main>

<?php snippet('wide', ['page' => page('home')->children()->findByURI('home-wide-02')]) ?>

<div class="ui container p20" role="main">
  <div class="pt40 pb40 divided-2 reverse-mobile">
    <div class="left">
      <h2><?= $page->title2()->html() ?></h2>
      <div class="text pt20 pb20">
        <?= $page->content()->text2()->kirbytext() ?>
      </div>
    </div>
    <div class="right flex-center">
      <img src="<?=$page->content()->image2() ?> "/>
    </div>
  </div>
</div>

<?php snippet('wide', ['page' => page('home')->children()->findByURI('home-wide-03')]) ?>

<div class="ui container p20">
  <div class="pt40 pb40 divided-2 ">
    <div class="left flex-center">
      <img src="<?=$page->content()->image3() ?> "/>
    </div>
    <div class="right">
      <h2><?= $page->title3()->html() ?></h2>
      <div class="text pt20 pb20">
        <?= $page->content()->text3()->kirbytext() ?>
      </div>
    </div>
  </div>
</div>

<?php snippet('footer') ?>
