<?php snippet('header') ?>

<main class="ui container pt40 pb40" role="main">
  <h1><?= $page->title()->html() ?></h1>
  <div class="intro text">
    <?= $page->intro()->kirbytext() ?>
  </div>
  <hr />
</main>

<div class="ui container p20">
  <div class="logo-div">
    <img src="/assets/images/logo.png" />
  </div>
</div>
